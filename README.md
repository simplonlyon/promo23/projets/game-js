# Projet Jeu JS

Durée du projet : 2~ semaines

L'objectif de ce projet est de faire un petit jeu en Javascript sans framework.
Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.

Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).

## Organisation
* Choisir le thème et le type du jeu
* Faire 2-3 maquettes fonctionnelles de votre jeu
* Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder
* Créer une milestone (un Sprint) par semaine et y indiquer l'état dans lequel vous souhaitez que le jeu soit au bout de ce sprint, assigner les fonctionnalités du board à la milestone
* Pour chaque fonction/méthode, je fais la JSDoc

## Aspects techniques obligatoires
* Utilisation des objets Javascript d'une manière ou d'une autre (genre une ou deux interfaces, ça serait bien)
* Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.), l'idée est de travailler la séparation des données et de l'affichage

## Notes
*  Les jeux en temps réels sont plus compliqués à gérer (genre un truc de plateforme, ou n'importe quel jeu où il se passe des choses sans action utilisateur·ice)
*  Les frameworks de jeu type phaserJS, bien qu'intéressant, ne sont pas autorisés car compétences trop spécifiques
*  Le jeu ne doit pas nécessairement être responsive (et c'est bien le seul projet qui ne le sera pas)

## Exemples de jeux
Pour les personnes qui n'ont pas d'idée voici quelques exemples de jeux faisables :
* Un Tamagochi
* Un Pokemon Battle simplifié (même principe que le tamagochi en fait, mais avec 2 persos)
* Un memory
* Un puissance 4
